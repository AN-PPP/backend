package ppp.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppp.demo.exceptions.CannotTakeBreakException;
import ppp.demo.model.AppUser;
import ppp.demo.model.BreakLog;
import ppp.demo.model.UserQueue;
import ppp.demo.repository.AppUserRepository;
import ppp.demo.repository.BreakLogRepository;
import ppp.demo.repository.UserQueueRepository;
import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserQueueService implements IUserQueueService {

    @Autowired
    private UserQueueRepository repository;

    @Autowired
    private AppUserRepository appUserRepository;

    @Autowired
    private BreakLogRepository breakLogRepository;

    @Override
    public Optional<UserQueue> findByName(String short_breaks) {
        return repository.findByName(short_breaks);
    }

    @Override
    public void addToQueue(String breaks, Long userId) {
        Optional<UserQueue> queueOpt = repository.findByName(breaks);
        if (queueOpt.isPresent()) {
            UserQueue queue = queueOpt.get();

            Optional<AppUser> userOpt = appUserRepository.findById(userId);
            if (userOpt.isPresent()) {
                AppUser user = userOpt.get();
                queue.getAppUserList().add(user);
                user.getQueues().add(queue);

                repository.saveAndFlush(queue);
                appUserRepository.saveAndFlush(user);
            }
        }
    }

    @Override
    public void removeToQueue(String breaks, Long userId) {
        Optional<UserQueue> queueOpt = repository.findByName(breaks);
        if (queueOpt.isPresent()) {
            UserQueue queue = queueOpt.get();
            Optional<AppUser> userOpt = appUserRepository.findById(userId);
            if (userOpt.isPresent()) {
                AppUser user = userOpt.get();
                for (BreakLog log : queue.getBreakLogs()) {
                    if (log.getWho() == user) {
                        queue.getBreakLogs().remove(log);
                        queue.setAvailableBreaks(queue.getAvailableBreaks() + 1);
                        log.setBreakEnd(LocalDateTime.now());
                        log.setQueue(null);
                        break;
                    }
                }
                repository.saveAndFlush(queue);
                appUserRepository.saveAndFlush(user);
                return;
            }
        }
        throw new CannotTakeBreakException();
    }

    @Override
    public void takeBreak(String breakType, Long userId) {
        AppUser user;
        Optional<AppUser> userOpt = appUserRepository.findById(userId);
        if (userOpt.isPresent()) {
            user = userOpt.get();
        } else {
            throw new CannotTakeBreakException();
        }

        Optional<UserQueue> queueOpt = repository.findByName(breakType);
        if (queueOpt.isPresent()) {
            UserQueue queue = queueOpt.get();
            if (queue.getAvailableBreaks() > 0 && !queue.getBreakLogs().stream().anyMatch(ap -> ap.getWho().equals(user))) {

                queue.setAvailableBreaks(queue.getAvailableBreaks() - 1);

                int breakMinutes = queue.getBreakTimeInMinutes();

                BreakLog breakLog = new BreakLog(LocalDateTime.now(), LocalDateTime.now().plusMinutes(breakMinutes), user, queue);

                breakLogRepository.save(breakLog);
                queue.getBreakLogs().add(breakLog);
                repository.saveAndFlush(queue);
                return;
            }
        }

        throw new CannotTakeBreakException();
    }

}
