package ppp.demo.service;

import ppp.demo.exceptions.RegistrationException;
import ppp.demo.exceptions.UserDoesNotExistException;
import ppp.demo.model.AppUser;
import ppp.demo.model.dto.LoginDto;
import ppp.demo.model.dto.PageResponse;

import java.util.Optional;

public interface IAppUserService {
    void register(AppUser appUser) throws RegistrationException;

    PageResponse<AppUser> getUsers(int page);

    PageResponse<AppUser> getAllUsers();

    Optional<AppUser> getUserWithLoginAndPassword(LoginDto dto) throws UserDoesNotExistException;

}
