package ppp.demo.service;

import ppp.demo.model.dto.BreakLogDto;

import java.util.List;

public interface IBreakLogService {
    public List<BreakLogDto> getShortBreaks();
}