package ppp.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ppp.demo.model.BreakLog;
import ppp.demo.model.dto.BreakLogDto;
import ppp.demo.repository.BreakLogRepository;
import ppp.demo.repository.UserQueueRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BreakLogService implements IBreakLogService {

    @Autowired
    private UserQueueRepository userQueueRepository;

    @Autowired
    private BreakLogRepository breakLogRepository;

    public List<BreakLogDto> getShortBreaks() {
        List<BreakLog> breakLogList = userQueueRepository.findByName("SHORT_BREAKS").get().getBreakLogs();

        return breakLogList.stream().map(breakLog -> new BreakLogDto(
                breakLog.getWho().getId(),
                breakLog.getWho().getLogin(),
                Duration.between(LocalDateTime.now(), breakLog.getBreakLimit()).getSeconds())).collect(Collectors.toList());
//        return breakLogList;
    }

    public List<BreakLogDto> getLongBreaks() {
        List<BreakLog> breakLogList = userQueueRepository.findByName("LONG_BREAKS").get().getBreakLogs();

        return breakLogList.stream().map(breakLog -> new BreakLogDto(
                breakLog.getWho().getId(),
                breakLog.getWho().getLogin(),
                Duration.between(LocalDateTime.now(), breakLog.getBreakLimit()).getSeconds())).collect(Collectors.toList());
    }
}