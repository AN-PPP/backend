package ppp.demo.service;

import ppp.demo.model.UserQueue;

import java.util.Optional;

public interface IUserQueueService {
    Optional<UserQueue> findByName(String short_breaks);

    void addToQueue(String long_breaks, Long userId);

    void removeToQueue(String short_breaks, Long userId);

    void takeBreak(String short_breaks, Long userId);
}
