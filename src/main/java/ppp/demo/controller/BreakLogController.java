package ppp.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ppp.demo.model.dto.RespFactory;
import ppp.demo.service.BreakLogService;

@RestController
@CrossOrigin
@RequestMapping("/breakTime/")
public class BreakLogController {

    @Autowired
    private BreakLogService breakLogService;

    @RequestMapping(path = "/shorts")
    public ResponseEntity getTakenShortBreaks(){
        return RespFactory.result(breakLogService.getShortBreaks());
    }

    @RequestMapping(path = "/longs")
    public ResponseEntity getTakenLongBreaks(){
        return RespFactory.result(breakLogService.getLongBreaks());
    }
}

