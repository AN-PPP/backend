package ppp.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ppp.demo.model.UserQueue;
import ppp.demo.model.dto.RespFactory;
import ppp.demo.service.IUserQueueService;

import javax.websocket.server.PathParam;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/queue/")
public class UserQueueController {

    @Autowired
    private IUserQueueService userQueueService;

    @RequestMapping(path = "/short")
    public ResponseEntity<UserQueue> getShortQueue() {
        Optional<UserQueue> optional = userQueueService.findByName("SHORT_BREAKS");
        if (optional.isPresent()) {
            return ResponseEntity.ok(optional.get());
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(path = "/long", method = RequestMethod.GET)
    public ResponseEntity<UserQueue> getLongQueue() {
        Optional<UserQueue> optional = userQueueService.findByName("LONG_BREAKS");
        if (optional.isPresent()) {
            return ResponseEntity.ok(optional.get());
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    @RequestMapping(path = "/addToLongQueue")
    public ResponseEntity addToLongQueue(@RequestParam(name = "userid") Long userId) {
        userQueueService.addToQueue("LONG_BREAKS", userId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/addToShortQueue")
    public ResponseEntity addToShortQueue(@RequestParam(name = "userid") Long userId) {
        userQueueService.addToQueue("SHORT_BREAKS", userId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/removeToLongQueue")
    public ResponseEntity removeToLongQueue(@RequestParam(name = "userid") Long userId) {
        userQueueService.removeToQueue("LONG_BREAKS", userId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/removeToShortQueue")
    public ResponseEntity removeToShortQueue(@RequestParam(name = "userid") Long userId) {
        userQueueService.removeToQueue("SHORT_BREAKS", userId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "/takeShortBreak")
    public ResponseEntity takeShortBreak(@RequestParam(name = "userid") Long userId){
        userQueueService.takeBreak("SHORT_BREAKS",userId);

        return RespFactory.ok("OK");
    }

    @RequestMapping(path = "/takeLongBreak")
    public ResponseEntity takeLongBreak(@RequestParam(name = "userid") Long userId){
        userQueueService.takeBreak("LONG_BREAKS",userId);

        return RespFactory.ok("OK");
    }

}
