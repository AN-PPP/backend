package ppp.demo.controller;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ppp.demo.exceptions.UserDoesNotExistException;
import ppp.demo.model.AppUser;
import ppp.demo.model.Role;
import ppp.demo.model.dto.AuthenticationDto;
import ppp.demo.model.dto.LoginDto;
import ppp.demo.model.dto.RespFactory;
import ppp.demo.service.IAppUserService;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import static ppp.demo.configuration.JWTFilter.AUTHORITIES_KEY;
import static ppp.demo.configuration.JWTFilter.SECRET;

@RestController
@CrossOrigin
public class AuthorizationController {

    @Autowired
    private IAppUserService appUserService;

    @RequestMapping(path = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<AuthenticationDto> authenticate(@RequestBody LoginDto dto) {
        try {
            Optional<AppUser> appUserOptional = appUserService.getUserWithLoginAndPassword(dto);
            AppUser user = appUserOptional.get();
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
            //We will sign our JWT with our ApiKey secret
            byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET);
            Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
            String token = Jwts.builder()
                    .setSubject(user.getLogin())
                    .setIssuedAt(new Date())
                    .claim(AUTHORITIES_KEY, translateRoles(user.getRoleSet())) // todo: do zmiany na getRoles?
                    .signWith(signatureAlgorithm, signingKey)
                    .compact();
            return RespFactory.result(new AuthenticationDto(token, user));
        } catch (UserDoesNotExistException e) {
            e.printStackTrace();
        }
        return RespFactory.badRequest();
    }
    private Set<String> translateRoles(Set<Role> roles) {
        return roles.stream().map(role -> role.getName()).collect(Collectors.toSet());
    }

}
