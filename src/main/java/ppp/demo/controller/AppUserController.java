package ppp.demo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ppp.demo.exceptions.RegistrationException;
import ppp.demo.model.AppUser;
import ppp.demo.model.dto.PageResponse;
import ppp.demo.model.dto.RespFactory;
import ppp.demo.model.dto.Response;
import ppp.demo.service.AppUserService;

@RestController
@CrossOrigin
@RequestMapping("/user/")
public class AppUserController {
    private AppUserService appUserService;
    @Autowired
    public AppUserController(AppUserService appUserService) {
        this.appUserService = appUserService;
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST)
    public ResponseEntity<Response> register(@RequestBody AppUser appUser) {
        try {
            appUserService.register(appUser);
        } catch (RegistrationException e) {
            return RespFactory.badRequest();
        }
        return RespFactory.created();
    }

    @RequestMapping(path = "/list", method = RequestMethod.GET)
    public ResponseEntity<Response> list() {
        PageResponse<AppUser> list = appUserService.getAllUsers();
        return RespFactory.result(list);
    }
}

