package ppp.demo.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ppp.demo.model.AppUser;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthenticationDto {
    private String token;
    private AppUser user;
}
