package ppp.demo.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BreakLogDto {
    private long userId;
    private String login;
    private long secondsLeft;
}
