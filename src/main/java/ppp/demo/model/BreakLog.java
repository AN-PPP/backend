package ppp.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class BreakLog {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private LocalDateTime breakStart;
    private LocalDateTime breakEnd;
    private LocalDateTime breakLimit;

    @ManyToOne
    private AppUser who;

    @ManyToOne
    @JsonBackReference
    private UserQueue queue;

    public BreakLog(LocalDateTime breakStart, LocalDateTime limit, AppUser who, UserQueue queue) {
        this.breakStart = breakStart;
        this.breakLimit = limit;
        this.who = who;
        this.queue = queue;
    }
}