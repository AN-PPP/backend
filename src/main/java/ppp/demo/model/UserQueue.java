package ppp.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class UserQueue {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private int availableBreaks;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private int breakTimeInMinutes;

    @OneToMany(mappedBy = "queue")
    private List<BreakLog> breakLogs;

    @ManyToMany(mappedBy = "queues", cascade = CascadeType.PERSIST)
    private List<AppUser> appUserList;

    public UserQueue(String name, int breakTimeInMinutes) {
        this.name = name;
        this.breakTimeInMinutes = breakTimeInMinutes;
        this.availableBreaks = 5;
    }
}
