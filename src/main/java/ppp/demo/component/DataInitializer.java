package ppp.demo.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ppp.demo.model.AppUser;
import ppp.demo.model.Role;
import ppp.demo.model.UserQueue;
import ppp.demo.repository.AppUserRepository;
import ppp.demo.repository.RoleRepository;
import ppp.demo.repository.UserQueueRepository;

@Component
public class DataInitializer {

    private RoleRepository roleRepository;
    private AppUserRepository appUserRepository;
    private UserQueueRepository userQueueRepository;

    @Autowired
    public DataInitializer(RoleRepository roleRepository, AppUserRepository appUserRepository, UserQueueRepository userQueueRepository) {
        this.roleRepository = roleRepository;
        this.appUserRepository = appUserRepository;
        this.userQueueRepository = userQueueRepository;
        loadData();
    }

    private void loadData() {
        Role adminRole = new Role("ADMIN");
        adminRole = roleRepository.saveAndFlush(adminRole);
        roleRepository.save(new Role("USER"));
        roleRepository.save(new Role("GUEST"));

        appUserRepository.save(new AppUser("admin", "admin", adminRole));

        userQueueRepository.save(new UserQueue("SHORT_BREAKS", 10));
        userQueueRepository.save(new UserQueue("LONG_BREAKS", 20));
    }
}