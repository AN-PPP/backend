package ppp.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ppp.demo.model.BreakLog;
import ppp.demo.model.UserQueue;

import java.util.List;

public interface BreakLogRepository extends JpaRepository<BreakLog, Long> {
    List<BreakLog> getAllByQueue(UserQueue queue);
}
