package ppp.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ppp.demo.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}