package ppp.demo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ppp.demo.model.AppUser;

import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByLogin(String username);

    Optional<AppUser> findByEmail(String email);

    Page<AppUser> findAllBy(Pageable pageable);

   }
