package ppp.demo.repository;

import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import ppp.demo.model.UserQueue;

import java.util.Optional;

public interface UserQueueRepository extends JpaRepository<UserQueue, Long>{
    Optional<UserQueue> findByName(String name);
}
